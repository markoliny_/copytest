import java.io.*;
import java.util.Scanner;

public class CopyTest {

    private static void copyFiles(File srcFile, File destFile) {
        try (FileInputStream inputStream = new FileInputStream(srcFile);
             FileOutputStream outputStream = new FileOutputStream(destFile)) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Can't find file: " + srcFile.getPath());
        } catch (IOException e) {
            System.out.println("Can't write date to: " + destFile.getPath());
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите путь до файла, который хотите копировать:");
        Scanner scanPath = new Scanner(System.in);
        File sourceFile = new File(scanPath.nextLine());

        System.out.println("Введите путь до папки, куда копировать файл:");
        Scanner outputPath = new Scanner(System.in);
        File newFile = new File(outputPath.nextLine() + "/" + sourceFile.getName());

        boolean needToCopy = true;
        if (newFile.exists()) {
            System.out.println("Файл с таким название уже существует. Заменить его?(yes/no)");
            Scanner say = new Scanner(System.in);
            String no = say.nextLine().trim().toLowerCase();

            if (no.equals("no")) {
                needToCopy = false;
                System.out.println("The file is not copied.");
            }
        }
        if (needToCopy) {
            copyFiles(sourceFile, newFile);
        }
    }
}
